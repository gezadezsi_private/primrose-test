#!/usr/bin/env bash

# IMPORTANT: set your hostname to match the config hostname into the Vagranfile

## --- SERVERNAME SECTION ---
## Set your virtual server name for APACHE here replacing myservername.virtuals to what you want
## IN FUTURE: this will change for a more smart command
## WARNING: don't use servername endings in .localhost due to a bug in chrome
sudo replace SERVERNAME primrose.dev -- /etc/apache2/sites-available/laravel.conf
sudo service apache2 restart

# Remove xdebug (See https://getcomposer.org/xdebug)
sudo apt-get -y remove php5-xdebug

## --- PROJECT SETUP ---
## customize this section to insert the command necessary to configure your Laravel project like:
##  - setting database
## The Database is on Apollo machine
##  - generate an .env file for local setup
##  - run composer
sudo composer self-update
#composer install
#composer update
#   - run (if necessary) artisan commands
#php artisan key:generate
#php artisan vendor:publish
#php artisan migrate